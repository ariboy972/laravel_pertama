<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index() {
        $pertanyaan = DB::table('pertanyaan')->get();
        //dd($pertanyaan);
        return view('pertanyaan.index',compact('pertanyaan'));
    }

    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
       // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'question' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["question"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil di simpan');
    }

    public function show($id) {
        $show_id = DB::table('pertanyaan')->where('id', $id)->first();
        //dd($show_id);
        return view('pertanyaan.show', compact('show_id'));
    }

    public function edit($id) {
        $edit_id = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('edit_id'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'question' => 'required'
        ]);

        $update_id = DB::table('pertanyaan')
                        ->where('id', $id)
                        ->update([
                            'judul' => $request['judul'],
                            'isi' => $request['question']
                        ]);
        return redirect('/pertanyaan')->with('success', "Berhasil Update Question $id");
    }

    public function destroy($id) {
        $delete = DB::table('pertanyaan')
                      ->where ('id', $id)
                      ->delete();
        return redirect('/pertanyaan')->with('success', "ID $id Berhasil di Hapus");
    }
}
